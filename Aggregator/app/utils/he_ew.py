import base64
import logging
import os
import subprocess
import time
from datetime import datetime
from pathlib import Path

import numpy

from config.flask_config import APP_ROOT, CIPHERTEXT_SAVE_FILE


class HomomorphicEncryptionEW:
    """
    Helper class used to setup the Decryption Module and handle aggregation and weight saving
    """
    max_range = 10.0
    supported_max_int = 60000

    def __init__(self, cipher_save_path, lib_executable: str):
        """
        Setup the HomomorphicEncryptionEW Module
        :param cipher_save_path: Filepath to temporarily store the ciphertext
        :param lib_executable: String Filepath which indicates the executable location of
                the app_HE executable of HE_EW_CPP
        """
        self._cipher_save_path = cipher_save_path
        self._encrypted_weights = []
        self.metadata = ""
        self._lib_executable_path_string = lib_executable

    @staticmethod
    def get_param_info():
        """
        Provides information regarding the scheme used in the module.
        This also includes the publicly known parameters used to set up the scheme.
        For instance, the supported maximum integer used for the encoding of the weight values
        :return: json response with content described above
        """
        res = {
            "scheme": "HomomorphicEncryptionEW"
        }
        return res

    def save_encrypted_weight(self, weights):
        """
        Save the encrypted weight from the worker
        :param weights: Layer weights of the ML Model
        :return: None
        """
        file_path = Path(self._cipher_save_path).joinpath(str(datetime.now()) + ".bin")
        with open(file_path, "wb+") as f:
            f.write(base64.b64decode(weights))
        self._encrypted_weights.append(file_path.absolute())

    def aggregate_encrypted_weights(self):
        """
        Adds all the weights currently saved in the aggregator
        :return: Resulting aggregated encrypted weights
        """
        start_time = time.perf_counter()
        inp_str = self.metadata
        num_party = len(self._encrypted_weights)
        if num_party == 0:
            time_elapsed = time.perf_counter() - start_time
            return {
                "metadata": self.metadata,
                "weights": None,
                "num_party": num_party,
                "aggregation_time": time_elapsed.__str__(),
            }
        result_file = Path(self._cipher_save_path).joinpath(CIPHERTEXT_SAVE_FILE)
        process = subprocess.run(
            [
                self._lib_executable_path_string,
                "add",
                result_file.absolute(),
                str(num_party),
                *self._encrypted_weights
            ],
            input=inp_str.encode('utf-8'),
            stdout=subprocess.PIPE)
        process_outputs = process.stdout.decode('utf-8')

        time_elapsed = time.perf_counter() - start_time
        logging.info(f"Time taken for encrypted aggregation is {time_elapsed} s")

        # Reset encrypted weights
        for i in self._encrypted_weights:
            file = Path(i)
            file.unlink()
        self._encrypted_weights = []

        with open(result_file, "rb") as f:
            encoded_string = base64.b64encode(f.read()).decode('utf-8')

        return {
            "metadata": process_outputs,
            "weights": encoded_string,
            "num_party": num_party,
            "aggregation_time": time_elapsed,
        }
