import logging

import keras
import numpy
import tensorflow as tf
import os
import requests
from keras.models import Model


class ModelNN:
    """
    Helper Class to define model and provide logical interfaces to the model
    """
    def __init__(self, h5_file, model_save_path):
        """
        Setup the model to be used
        :param h5_file: H5 File location to save the local model weights
        :param model_save_path: Save path of the global model taken from the server
        """
        # Setup model to be trained

        with open(model_save_path, 'wb+') as f:
            f.write(h5_file)

        # This is our model definition taken from server
        self.model = tf.keras.models.load_model(model_save_path)

    def set_weights(self, weights):
        """
        Set the weights of the model
        :param weights: List containing Weights of the layers
        :return: None
        """
        self.model.set_weights(weights)

    def get_weights(self):
        """
        Get the weights of the model
        :return: Model Weights values as a list of numpy arrays.
        """
        return self.model.get_weights()

    def train(self, x_train, y_train):
        """
        Train the model
        :param x_train: Training data to be used as inputs for the model training
        :param y_train: Training labels for the model training
        :return: History of the model training
        """
        # Scale images to the [0, 1] range
        x_train = x_train.astype("float32") / 255

        # Make sure images have shape (28, 28, 1)
        x_train = numpy.expand_dims(x_train, -1)

        print("x_train shape:", x_train.shape)
        print(x_train.shape[0], "train samples")

        # convert class vectors to binary class matrices
        num_classes = 10
        y_train = keras.utils.to_categorical(y_train, num_classes)

        history = self.model.fit(
            x_train,
            y_train,
            batch_size=128,
            epochs=1,
            # We pass some validation for
            # monitoring validation loss and metrics
            # at the end of each epoch
            validation_split=0.1
        )
        logging.info("Finished training for 1 epoch!")
