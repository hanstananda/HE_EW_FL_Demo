import logging

import keras
import numpy as np


class LoaderMNIST:
    """
    Helper class used to prepare and load the dataset for the workers.
    This class may not be used when folder loading is used to replace the builtin dataset.
    """
    def __init__(self, num_partition):
        """
        Load the dataset and partition it based on the number of workers involved
        :param num_partition:
        """
        (x_train, y_train), (_, _) = keras.datasets.mnist.load_data()
        logging.info(f"Total Dataset size={x_train.shape}")
        self.x_partition = np.array_split(x_train, num_partition)
        self.y_partition = np.array_split(y_train, num_partition)

    def get_train_data_partitions(self, idx):
        """
        Function to get the partitioned data based on the id partition
        :param idx: ID partition to use
        :return: The dataset partition to be used by the worker
        """

        return self.x_partition[idx-1], self.y_partition[idx-1]
