import base64
import logging
import os
import subprocess
import time
from pathlib import Path

import numpy

from config.flask_config import APP_ROOT


class HomomorphicEncryptionEW:
    max_range = 10.0
    supported_max_int = 60000

    def __init__(self, public_save_path, cipher_save_path, lib_executable: str):
        """
        Setup the HomomorphicEncryptionEW Module
        :param public_save_path: Filepath to save the public key
        :param cipher_save_path: Filepath to temporarily store the ciphertext
        :param lib_executable: String Filepath which indicates the executable location of
                the app_HE executable of HE_EW_CPP
        """
        self._public_save_path = public_save_path
        self._cipher_save_path = cipher_save_path
        self._lib_executable_path_string = lib_executable

    @classmethod
    def get_param_info(cls):
        """
        Provides information regarding the scheme used in the module.
        This also includes the publicly known parameters used to set up the scheme.
        For instance, the supported maximum integer used for the encoding of the weight values
        :return: json response with content described above
        """
        res = {
            "scheme": "HomomorphicEncryptionEW",
            "supported_max_int": cls.supported_max_int,
            "max_range": cls.max_range,
        }
        return res

    def normalize_and_encode(self, val):
        """
        Encode the value from a floating value of (-max_range, max_range) to (0, 2*supported_max_int)
        :param val: Value to be encoded. In this case, a value from the layer weight
        :return: Integer value representing the encoded value of the input value
        """
        processed_val = max(val, -self.max_range)
        processed_val = min(processed_val, self.max_range)
        # Normalize from (-max_range, max_range) to (0, 2*max_range)
        processed_val += self.max_range

        processed_val *= self.supported_max_int
        processed_val /= 2 * self.max_range
        return int(processed_val)

    def encrypt_layer_weights(self, layer_weights):
        """
        Encrypt weights layer by layer. The original weight will be transformed to 1D vector to match the supported
        dimension of the HomomorphicEncryptionEW Library.
        :param layer_weights:
        :return: JSON response containing the encrypted weights
        """
        weights = []
        start_time = time.perf_counter()

        for idx, layer_weight in enumerate(layer_weights):
            logging.debug("layer weight {} = {}".format(idx, layer_weight))
            logging.debug(layer_weight.shape)
            # Reshape to 1D vector
            vector_size = layer_weight.size
            reshaped_layer_weight = numpy.resize(layer_weight, (vector_size,))
            logging.debug(f"Encrypting layer weight {idx} = {numpy.amin(reshaped_layer_weight)} " +
                          f"{numpy.amax(reshaped_layer_weight)} {reshaped_layer_weight.shape}")
            weights.append(reshaped_layer_weight)

        # Format input to be given to the library
        # inp_str = f"1 {self._public_save_path}\n"
        inp_str = f"{len(weights)}\n"
        for layer in weights:
            inp_str += f"{len(layer)} "
            for val in layer:
                processed_val = self.normalize_and_encode(val)
                inp_str += f"{processed_val} "
            inp_str += "\n"

        process = subprocess.run(
            [
                self._lib_executable_path_string,
                "encrypt",
                self._cipher_save_path.absolute(),
                self._public_save_path.absolute()
            ],
            input=inp_str.encode('utf-8'),
            stdout=subprocess.PIPE
        )

        process_outputs = process.stdout.decode('utf-8')

        with open(self._cipher_save_path, "rb") as f:
            encoded_string = base64.b64encode(f.read()).decode('utf-8')

        time_elapsed = time.perf_counter() - start_time
        logging.info(f"Time taken for encryption and encoding is {time_elapsed} s")

        return {
            "metadata": process_outputs,
            "weights": encoded_string,
            "encryption_time": time_elapsed,
        }
