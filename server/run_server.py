"""
Main script used to start the Server
"""

import argparse
import logging
from gevent.pywsgi import WSGIServer

from app import create_app
from config.flask_config import DockerConfig, DefaultConfig

parser = argparse.ArgumentParser(description='Run the server service')

parser.add_argument('--is_docker', default=False, action='store_true',
                    help='specify whether docker IPs should be used')

results = parser.parse_args()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    if results.is_docker:
        config = DockerConfig
    else:
        config = DefaultConfig
    app = create_app(config)
    app_server = WSGIServer(('0.0.0.0', 7000), app)
    app_server.serve_forever()
