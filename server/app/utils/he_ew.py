import base64
import logging
import os
import subprocess
import time
from pathlib import Path

import numpy

from config.flask_config import APP_ROOT


class HomomorphicEncryptionEW:
    """
    Helper class used to setup the Decryption Module and handle decryption & decoding logic
    """
    max_range = 10.0
    supported_max_int = 60000
    EPS = 1e-3

    def __init__(self, private_key_save_path, cipher_save_path, lib_executable: str):
        """
        Setup the HomomorphicEncryptionEW Module
        :param private_key_save_path: Filepath to save the private key generated
        :param cipher_save_path: Filepath to temporarily store the ciphertext
        :param lib_executable: String Filepath which indicates the executable location of
                the app_HE executable of HE_EW_CPP
        """
        self._cipher_save_path = cipher_save_path
        self._private_key_save_path = private_key_save_path
        self._lib_executable_path_string = lib_executable

    @classmethod
    def get_param_info(cls):
        """
        Provides information regarding the scheme used in the module.
        This also includes the publicly known parameters used to set up the scheme.
        For instance, the supported maximum integer used for the encoding of the weight values
        :return: json response with content described above
        """
        res = {
            "scheme": "HomomorphicEncryptionEW",
            "supported_max_int": cls.supported_max_int,
            "max_range": cls.max_range,
        }
        return res

    def decode_value(self, val: int, num_party: int):
        """
        Function to decode the value from integer representation back to floating representation
        :param val: Integer value to be decoded
        :param num_party: Number of parties involved in the aggregation
        :return: The actual floating value of the encoded integer. In this case, actual weight value of the model
        """
        # if val >= 2 * self.supported_max_int:
        #     logging.warning(f"Found decrypted value of {val} more than supported limit!")
        processed_val = val * 2 * self.max_range
        processed_val /= self.supported_max_int

        # Divide by the number of involved parties
        result = processed_val / num_party

        # Normalize back from (0, 2*max_range) to (-max_range, max_range)
        result -= self.max_range

        if result - self.EPS > self.max_range or result + self.EPS < -self.max_range:
            logging.warning(f"Found decoded value of {result} from {val} more than supported limit! Resetting to 0...")
            result = 0.0

        return result

    def decrypt_layer_weights(self, metadata, layer_weights, num_party):
        """
        Function to decode and decrypt the encrypted weight layers obtained from the aggregator.
        The decoding will be performed by calling another internal function within this module.
        :param metadata: Information provided regarding the layer weight slicing organization
        :param layer_weights: Aggregated encrypted weight from the aggregator
        :param num_party: The number of parties involved in this aggregated weights
        :return: Decrypted weights for the model
        """
        decoded_weights = []
        inp_str = metadata

        file_path = Path(self._cipher_save_path)
        secret_path = Path(self._private_key_save_path)

        with open(file_path, "wb+") as f:
            f.write(base64.b64decode(layer_weights))

        process = subprocess.run([self._lib_executable_path_string, "decrypt", file_path.absolute(), secret_path.absolute()],
                                 input=inp_str.encode('utf-8'),
                                 stdout=subprocess.PIPE)
        process_outputs = process.stdout.decode('utf-8').split("\n")

        for idx in range(len(process_outputs)):
            encoded_weights = process_outputs[idx].split()[1:]
            decoded = [self.decode_value(int(i), num_party=num_party) for i in encoded_weights]
            if decoded:  # Check nonempty
                decoded_weights.append(decoded)
                # logging.warning(decoded[:10])

        return decoded_weights
