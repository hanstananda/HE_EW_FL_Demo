"""
All flask configurations shall be put here
"""

import os
from pathlib import Path

basedir = os.path.abspath(os.path.dirname(__file__))
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

MODEL_SAVE_FILE = "output/demo_model.h5"
PARAMS_SAVE_FILE = "output/params.txt"
CIPHERTEXT_SAVE_FILE = "cipher.txt"
SECRET_KEY_SAVE_FILE = "sk.bin"
PUBLIC_KEY_SAVE_FILE = "pk.bin"


class DefaultConfig:
    """Base config."""
    SERVER_IP = "http://localhost:7000"
    executable_relative_path = "library/app_HE"
    LIBRARY_EXECUTABLE = str(Path(APP_ROOT).parent.joinpath(executable_relative_path).absolute())


class DockerConfig(DefaultConfig):
    SERVER_IP = "http://he-ew-demo-server:7000"
    LIBRARY_EXECUTABLE = "app_HE"
